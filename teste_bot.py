import requests

cookies = {
    'cert_Origin': 'www.google.com.br',
    '_gcl_au': '1.1.1918943568.1537555350',
    '_ga': 'GA1.3.465815249.1537555350',
    '_gid': 'GA1.3.1790155338.1537555350',
    'IM.Hash': 'cfcd208495d565ef66e7dff9f98764da',
    '__gads': 'ID=b2b2377372debc60:T=1537555349:S=ALNI_MZm7K3A1HcRT0yjSuFcOmj4ObawSA',
    '___ws-sr': 'https://www.google.com.br/',
    'nvg22862': '92fa3e391a08e4e90d481c06909|2_265',
    '__qca': 'P0-1860299536-1537555352102',
    'ortcsession-w5tlOg-s': '33c391b0c241f63d',
    'ortcsession-w5tlOg': '33c391b0c241f63d',
    '__hstc': '254361559.86b98e848614ed684db41b44bdae77bc.1537555359499.1537555359499.1537555359499.1',
    '__hssrc': '1',
    'hubspotutk': '86b98e848614ed684db41b44bdae77bc',
    '___ws_d_st': '{}',
    'tt_c_vmt': '1537555360',
    'tt_c_c': 'search',
    'tt_c_s': 'search',
    'tt_c_m': 'search',
    '__utma': '254361559.465815249.1537555350.1537555360.1537555360.1',
    '__utmc': '254361559',
    '__utmz': '254361559.1537555360.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)',
    'tt.u': '7A0B000A997A505B780EFEB002415059',
    'tt.nprf': '70,81,36,20',
    '_ttdmp': 'E:2|S:18,9|A:4|X:3|C:1|U:11,10|LS:70,81,36,20|CA:CA3595',
    '_ttuu.s': '1537555685375',
    '___ws_ses': 'D6A5DAB323DB71A6.2',
    '___ws_vis': 'D6A5DAB323DB71A6.1537555685647',
    '___ws_ses_sec': '1594:1537555685647',
    '___ws_vis_sec': '1594:1537555685647',
    'InfoMoney.UserInfo': '%7b%22UserId%22%3anull%2c%22UserAnonymusId%22%3a%22525f802c-b09b-4c90-9b7f-9ff66a12bfa6%22%2c%22Profile%22%3a%22Visit%22%2c%22DateLastAccess%22%3a%222018-09-21T00%3a00%3a00-03%3a00%22%2c%22Navegg%22%3anull%2c%22Ip%22%3anull%2c%22Social%22%3anull%2c%22YearBirth%22%3anull%2c%22IsPartner%22%3afalse%2c%22Products%22%3anull%2c%22IsTailMatch%22%3afalse%2c%22TailHash%22%3anull%7d',
    'ws-refr': 'https://www.infomoney.com.br/cryptos/bitcoin',
    '__hssc': '254361559.2.1537555359499',
    '__utmb': '254361559.2.9.1537555762373',
}

headers = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7,ja;q=0.6',
}

response = requests.get('https://www.infomoney.com.br/cryptos/bitcoin', headers=headers, cookies=cookies)
print (response)